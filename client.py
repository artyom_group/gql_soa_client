from aiographql.client import GraphQLClient, GraphQLRequest
import asyncio


class Client:
    def __init__(self):
        self.client = GraphQLClient(endpoint="http://127.0.0.1:8000/graphql")
        self.scoreboards = {}

    def handler(self, data):
        print(data.json["payload"])

    async def subscribe_on_scoreboard(self, gameId):
        request = GraphQLRequest( query='\n'.join(["subscription($gameId:Int!) {",
                                               "\tscoreboard(gameId: $gameId) {",
                                               "\t\tplayers",
                                               "\t\tscore",
                                               "\t}",
                                               "}"]))
        subscription = await self.client.subscribe(request=request, variables={"gameId": gameId},
                                                   on_data=self.handler, protocols="graphql-ws")
        return subscription

    async def add_comment(self, author, text, gameId):
        request = GraphQLRequest( query='\n'.join(["mutation {",
                           f"\taddComment(gameId: {gameId}, comment: " + "{",
                           f"\t\tauthor: \"{author}\"",
                           f"\t\ttext: \"{text}\"",
                           "\t}) {",
                           "\t\ttext",
                           "\t}",
                           "}"]))
        response = await self.client.query(request=request)
        print(response.data)

    async def get_games(self):
        request = GraphQLRequest( query='\n'.join(["query {",
                                                   "\tgames {",
                                                   "\t\tid",
                                                   "\t\tactive",
                                                   "\t\tcomments {",
                                                   "\t\t\tauthor",
                                                   "\t\t\ttext",
                                                   "\t\t}"
                                                   "\t}",
                                                   "}"]))
        response = await self.client.query(request=request)
        print(response.data)


async def main():
    c = Client()
    while True:
        print("Enter:\n[1|author|text|gameId] to add comment\n[2] to get list of games\n[3|time|gameId] to monitor scoreboard for *time* seconds")
        s = input().split("|")
        if s[0] == "1":
            await c.add_comment(s[1], s[2], int(s[3]))
        elif s[0] == "2":
            await c.get_games()
        else:
            subscription = await c.subscribe_on_scoreboard(int(s[2]))
            await asyncio.wait([subscription.task], timeout=int(s[1]))

asyncio.run(main())
